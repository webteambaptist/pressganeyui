﻿var cahpsTable;

$(document).ready(function () {
    GenerateCAHPSResultTable();
});

function GenerateCAHPSResultTable() {
    cahpsTable = $('#cahpsResults').DataTable({
        "dom": '<"top"l>rt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": "/Home/GETCAHPSResults",
            "error": function (errResp) {
                alert("There was an issue processing your request. Please try again.");
            },            
            "dataSrc": function (json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are is currently no CAHPS Results."
        },
        "columns": [
            { "data": 'SurveyId', sDefaultContent: '' }, 
            { "data": 'ClientId', sDefaultContent: '' }, 
            { "data": 'Service', sDefaultContent: '' }, 
            { "data": 'Recdate', sDefaultContent: '' }, 
            { "data": 'Disdate', sDefaultContent: '' }, 
            { "data": 'Varname', sDefaultContent: '' }, 
            { "data": 'Value', sDefaultContent: '' }, 
            { "data": 'Itacctno', sDefaultContent: '' }, 
            { "data": 'QmService', sDefaultContent: '' }, 
            { "data": 'QmVarname', sDefaultContent: '' }, 
            { "data": 'QmQuestionTxt', sDefaultContent: '' }, 
            { "data": 'QmSection', sDefaultContent: '' }, 
            { "data": 'QmStandard', sDefaultContent: '' }, 
            { "data": 'QmScreening', sDefaultContent: '' }, 
            { "data": 'QmTopBoxScale', sDefaultContent: '' }, 
            { "data": 'QmTopBoxAnswer', sDefaultContent: '' }, 
        ],
        "columnDefs": [
            {
                searchable: false,
                targets: [10,11,12,13,14,15]
            }
        ],
        "pageLength": 10,
        "pagingType": "full_numbers",
        responsive: true,
    });
}

$("#searchbox").keyup(function () {
    cahpsTable.rows().search(this.value).draw();
});