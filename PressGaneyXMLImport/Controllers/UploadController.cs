﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;
using NLog.Config;
using NLog.Targets;
using PressGaneyXMLImport.Models;

namespace PressGaneyXMLImport.Controllers
{
    public class UploadController : Controller
    {
        private readonly PressGaneyContext _context;
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public UploadController(PressGaneyContext context)
        {
            _context = context;
        }
        
        public IActionResult UploadXML()
        {
            return View();
        }
        public IActionResult UploadQuestionsXML()
        {
            return View();
        }
        public IActionResult UploadCAHPSXML()
        {
            return View();
        }
        public IActionResult UploadDemographicsXML()
        {
            return View();
        }

        public IActionResult ResultsPage(string _resultMessage, bool resultStatus)
        {
            Results r = new Results();
            r.ResultMessage = _resultMessage;
            r.ResultStatus = resultStatus;
            return View(r);
        }

        [HttpPost]
        public IActionResult UploadFile(IFormFile file)
        {
        
        LoggingConfiguration config = new NLog.Config.LoggingConfiguration();
        FileTarget logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"PressGaneyUploadEntireXML-{DateTime.Now:MM-dd-yyyy}.log" };
        config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
        NLog.LogManager.Configuration = config;

            Logger.Info("Starting Upload Entire XML File.");
            var _resultMessage = "";
            bool resultStatus = false;
            try
            {
                if (file.Length > 0)
                {
                    var doc = new XmlDocument();
                    doc.Load(file.OpenReadStream());
                    #region Question Map Table
                    using (var q = _context.Database.BeginTransaction())
                    {
                        try
                        {
                            XmlNodeList _questionList = doc.SelectNodes("/DATA_EXPORT/HEADER/QUESTION_MAP/QUESTION");
                            Logger.Info("Starting Question_Map from Entire XML File.");
                            foreach (XmlNode xn in _questionList)
                            {
                                //var doesExist = _context.QuestionMap.Where(x => x.Service == xn["SERVICE"].InnerText && x.Varname == xn["VARNAME"].InnerText && x.QuestionText == xn["QUESTION_TEXT"].InnerText && x.Section == xn["SECTION"].InnerText && x.Standard == xn["STANDARD"].InnerText && x.Screening == xn["SCREENING"].InnerText && x.TopBoxScale == xn["TOP_BOX_SCALE"].InnerText && x.TopBoxAnswer == xn["TOP_BOX_ANSWER"].InnerText).FirstOrDefault();
                                var doesExist = _context.QuestionMap.Where(x => x.Service == xn["SERVICE"].InnerText && x.Varname == xn["VARNAME"].InnerText).FirstOrDefault();
                                if (doesExist == null)
                                {
                                    Logger.Info("New Question Found. VARNAME: " + xn["VARNAME"].InnerText);
                                    QuestionMap qmr = new QuestionMap();
                                    qmr.Service = xn["SERVICE"].InnerText;
                                    qmr.Varname = xn["VARNAME"].InnerText;
                                    qmr.QuestionText = xn["QUESTION_TEXT"].InnerText;
                                    qmr.Section = xn["SECTION"].InnerText;
                                    qmr.Standard = xn["STANDARD"].InnerText;
                                    qmr.Screening = xn["SCREENING"].InnerText;
                                    qmr.TopBoxScale = xn["TOP_BOX_SCALE"].InnerText;
                                    qmr.TopBoxAnswer = xn["TOP_BOX_ANSWER"].InnerText;
                                    qmr.DateTimeStamp = DateTime.Now;

                                    _context.QuestionMap.Add(qmr);
                                }
                                else
                                {
                                    Logger.Info("Updating Existing Question. VARNAME: " + xn["VARNAME"].InnerText);

                                    var data = _context.QuestionMap
                                        .FirstOrDefault(x => x.Service == xn["SERVICE"].InnerText &&
                                                             x.Varname == xn["VARNAME"].InnerText);

                                    data.Service = xn["SERVICE"].InnerText;
                                    data.Varname = xn["VARNAME"].InnerText;
                                    data.QuestionText = xn["QUESTION_TEXT"].InnerText;
                                    data.Section = xn["SECTION"].InnerText;
                                    data.Standard = xn["STANDARD"].InnerText;
                                    data.Screening = xn["SCREENING"].InnerText;
                                    data.TopBoxScale = xn["TOP_BOX_SCALE"].InnerText;
                                    data.TopBoxAnswer = xn["TOP_BOX_ANSWER"].InnerText;
                                    data.DateTimeStamp = DateTime.Now;
                                }
                            }
                            _context.SaveChanges();
                            q.Commit();
                        }
                        catch (Exception ex)
                        {
                            Logger.Error("Exception occurred during the question upload. :: " + ex.Message);
                            q.Rollback();
                            _resultMessage = "There was an issue inserting the Question Map Section of the XML. The remainder of the file wasnt inserted.";
                            resultStatus = false;
                            return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                        }
                    }
                    #endregion

                    #region Patient Level Data Cahps Response
                    try
                    {
                        XmlNodeList _patientLevelDataCAHPSList = doc.SelectNodes("/DATA_EXPORT/PATIENTLEVELDATA");
                        Logger.Info("Starting CAHPS/RESPONSE from Entire XML File.");
                        foreach (XmlNode item in _patientLevelDataCAHPSList)
                        {
                            using (var c = _context.Database.BeginTransaction())
                            {
                                try
                                {
                                    foreach (XmlNode child in item.SelectNodes("CAHPS/RESPONSE"))
                                    {
                                        var doesExist = _context.PatientleveldataCahpsResponse.Where(x => x.SurveyId == item["SURVEY_ID"].InnerText && x.ClientId == item["CLIENT_ID"].InnerText && x.Service == item["SERVICE"].InnerText && x.Recdate == item["RECDATE"].InnerText && x.Disdate == item["DISDATE"].InnerText && x.Varname == child["VARNAME"].InnerText && x.Value == child["VALUE"].InnerText).FirstOrDefault();
                                        if (doesExist == null)
                                        {
                                            Logger.Info("New CAHPS/RESPONSE Found. VARNAME: " + item["VARNAME"].InnerText + " :: SURVERY_ID: " + item["SURVEY_ID"].InnerText);

                                            PatientleveldataCahpsResponse results = new PatientleveldataCahpsResponse();
                                            results.SurveyId = item["SURVEY_ID"].InnerText;
                                            results.ClientId = item["CLIENT_ID"].InnerText;
                                            results.Service = item["SERVICE"].InnerText;
                                            results.Recdate = item["RECDATE"].InnerText;
                                            results.Disdate = item["DISDATE"].InnerText;
                                            results.DateTimeStamp = DateTime.Now;
                                            results.Varname = child["VARNAME"].InnerText;
                                            results.Value = child["VALUE"].InnerText;
                                            _context.PatientleveldataCahpsResponse.Add(results);
                                        }
                                    }
                                    _context.SaveChanges();
                                    c.Commit();
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error("Exception occurred during the CAHPS/RESPONSE upload. :: " + ex.Message);
                                    c.Rollback();
                                    continue;
                                }
                            }
                            #region Patient Level Data Demographics Response
                            using (var d = _context.Database.BeginTransaction())
                            {
                                try
                                {
                                    Logger.Info("Starting DEMOGRAPHICS/RESPONSE from Entire XML File.");
                                    foreach (XmlNode newchild in item.SelectNodes("DEMOGRAPHICS/RESPONSE"))
                                    {

                                        var doesExist = _context.PatientleveldataDemographicsResponse.Where(x => x.SurveyId == item["SURVEY_ID"].InnerText && x.ClientId == item["CLIENT_ID"].InnerText && x.Service == item["SERVICE"].InnerText && x.Recdate == item["RECDATE"].InnerText && x.Disdate == item["DISDATE"].InnerText && x.Varname == newchild["VARNAME"].InnerText && x.Value == newchild["VALUE"].InnerText).FirstOrDefault();

                                        if (doesExist == null)
                                        {
                                            Logger.Info("New DEMOGRAPHICS/RESPONSE Found. VARNAME: " + item["VARNAME"].InnerText + " :: SURVERY_ID: " + item["SURVEY_ID"].InnerText);

                                            PatientleveldataDemographicsResponse _demographicsResults = new PatientleveldataDemographicsResponse();
                                            _demographicsResults.SurveyId = item["SURVEY_ID"].InnerText;
                                            _demographicsResults.ClientId = item["CLIENT_ID"].InnerText;
                                            _demographicsResults.Service = item["SERVICE"].InnerText;
                                            _demographicsResults.Recdate = item["RECDATE"].InnerText;
                                            _demographicsResults.Disdate = item["DISDATE"].InnerText;
                                            _demographicsResults.DateTimeStamp = DateTime.Now;
                                            _demographicsResults.Varname = newchild["VARNAME"].InnerText;
                                            _demographicsResults.Value = newchild["VALUE"].InnerText;
                                            _context.PatientleveldataDemographicsResponse.Add(_demographicsResults);
                                        }
                                    }
                                    _context.SaveChanges();

                                    d.Commit();
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error("Exception occurred during the DEMOGRAPHICS/RESPONSE upload. :: " + ex.Message);
                                    d.Rollback();
                                    continue;
                                }
                            }
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Exception occurred during upload of the entire xml file. :: " + ex.Message);
                        _resultMessage = "There was an issue inserting the Data from the XML. The remainder of the file wasnt inserted.";
                        resultStatus = false;
                        return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                    }
                    #endregion                   
                    _resultMessage = "All of the data from the XML File has been inserted into the database successfully.";
                    resultStatus = true;
                    return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                }
                else
                {
                    _resultMessage = "File was not uploaded. Or the file was empty";
                    resultStatus = false;
                    return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred during file upload itself. :: " + e.Message);
                _resultMessage = "File was not uploaded. Or the file was empty";
                resultStatus = false;
                return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
            }
        }

        [HttpPost]
        public IActionResult UploadFileQuestions(IFormFile file)
        {
            LoggingConfiguration config = new NLog.Config.LoggingConfiguration();
            FileTarget logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"PressGaneyUploadQuestionsFromXML-{DateTime.Now:MM-dd-yyyy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;

            var _resultMessage = "";
            bool resultStatus = false;
            try
            {
                if (file.Length > 0)
                {
                    var doc = new XmlDocument();
                    doc.Load(file.OpenReadStream());
                    #region Question Map Table
                    using (var q = _context.Database.BeginTransaction())
                    {
                        try
                        {
                            XmlNodeList _questionList = doc.SelectNodes("/DATA_EXPORT/HEADER/QUESTION_MAP/QUESTION");
                            Logger.Info("Starting Question_Map Only from XML File.");
                            foreach (XmlNode xn in _questionList)
                            {
                                //var doesExist = _context.QuestionMap.Where(x => x.Service == xn["SERVICE"].InnerText && x.Varname == xn["VARNAME"].InnerText && x.QuestionText == xn["QUESTION_TEXT"].InnerText && x.Section == xn["SECTION"].InnerText && x.Standard == xn["STANDARD"].InnerText && x.Screening == xn["SCREENING"].InnerText && x.TopBoxScale == xn["TOP_BOX_SCALE"].InnerText && x.TopBoxAnswer == xn["TOP_BOX_ANSWER"].InnerText).FirstOrDefault();
                                var doesExist = _context.QuestionMap.Where(x => x.Service == xn["SERVICE"].InnerText && x.Varname == xn["VARNAME"].InnerText).FirstOrDefault();
                                if (doesExist == null)
                                {
                                    Logger.Info("New Question Found. VARNAME: " + xn["VARNAME"].InnerText);
                                    QuestionMap qmr = new QuestionMap();
                                    qmr.Service = xn["SERVICE"].InnerText;
                                    qmr.Varname = xn["VARNAME"].InnerText;
                                    qmr.QuestionText = xn["QUESTION_TEXT"].InnerText;
                                    qmr.Section = xn["SECTION"].InnerText;
                                    qmr.Standard = xn["STANDARD"].InnerText;
                                    qmr.Screening = xn["SCREENING"].InnerText;
                                    qmr.TopBoxScale = xn["TOP_BOX_SCALE"].InnerText;
                                    qmr.TopBoxAnswer = xn["TOP_BOX_ANSWER"].InnerText;
                                    qmr.DateTimeStamp = DateTime.Now;

                                    _context.QuestionMap.Add(qmr);
                                    //_context.SaveChanges();
                                }
                                else 
                                {
                                    Logger.Info("Updating Existing Question. VARNAME: " + xn["VARNAME"].InnerText);

                                    var data = _context.QuestionMap
                                            .FirstOrDefault(x => x.Service == xn["SERVICE"].InnerText &&
                                                                 x.Varname == xn["VARNAME"].InnerText);

                                        data.Service = xn["SERVICE"].InnerText;
                                        data.Varname = xn["VARNAME"].InnerText;
                                        data.QuestionText = xn["QUESTION_TEXT"].InnerText;
                                        data.Section = xn["SECTION"].InnerText;
                                        data.Standard = xn["STANDARD"].InnerText;
                                        data.Screening = xn["SCREENING"].InnerText;
                                        data.TopBoxScale = xn["TOP_BOX_SCALE"].InnerText;
                                        data.TopBoxAnswer = xn["TOP_BOX_ANSWER"].InnerText;
                                        data.DateTimeStamp = DateTime.Now;
                                }
                            }
                            _context.SaveChanges();
                            q.Commit();
                            _resultMessage = "The Question Map data from the XML File has been inserted into the database successfully.";
                            resultStatus = true;
                            return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                        }
                        catch (Exception e)
                        {
                            Logger.Error("Exception occurred during the question upload. :: " + e.Message);
                            q.Rollback();
                            _resultMessage = "There was an issue inserting the Question Map Section of the XML. The remainder of the file wasnt inserted.";
                            return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                        }
                    }
                    #endregion                               
                }
                else
                {
                    _resultMessage = "File was not uploaded. Or the file was empty. Please return to the upload page and try again.";
                    return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Exception occurred during file upload itself. :: " + ex.Message);
                _resultMessage = "File was not uploaded. Or the file was empty. Please return to the upload page and try again.";
                return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
            }
        }

        [HttpPost]
        public IActionResult UploadFileCAHPS(IFormFile file)
        {
            LoggingConfiguration config = new NLog.Config.LoggingConfiguration();
            FileTarget logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"PressGaneyUploadCAHPSFromXML" +
                $"-{DateTime.Now:MM-dd-yyyy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;

            var _resultMessage = "";
            bool resultStatus = false;
            try
            {
                if (file.Length > 0)
                {
                    var doc = new XmlDocument();
                    doc.Load(file.OpenReadStream());

                    #region Patient Level Data Cahps Response
                    try
                    {
                        XmlNodeList _patientLevelDataCAHPSList = doc.SelectNodes("/DATA_EXPORT/PATIENTLEVELDATA");
                        Logger.Info("Starting CAHPS/RESPONSE Only from XML File.");
                        foreach (XmlNode item in _patientLevelDataCAHPSList)
                        {
                            using (var c = _context.Database.BeginTransaction())
                            {
                                try
                                {
                                    foreach (XmlNode child in item.SelectNodes("CAHPS/RESPONSE"))
                                    {
                                        var doesExist = _context.PatientleveldataCahpsResponse.Where(x => x.SurveyId == item["SURVEY_ID"].InnerText && x.ClientId == item["CLIENT_ID"].InnerText && x.Service == item["SERVICE"].InnerText && x.Recdate == item["RECDATE"].InnerText && x.Disdate == item["DISDATE"].InnerText && x.Varname == child["VARNAME"].InnerText && x.Value == child["VALUE"].InnerText).FirstOrDefault();
                                        if (doesExist == null)
                                        {
                                            Logger.Info("New CAHPS/RESPONSE Found. VARNAME: " + child["VARNAME"].InnerText + " :: SURVERY_ID: " + item["SURVEY_ID"].InnerText);
                                            PatientleveldataCahpsResponse results = new PatientleveldataCahpsResponse();
                                            results.SurveyId = item["SURVEY_ID"].InnerText;
                                            results.ClientId = item["CLIENT_ID"].InnerText;
                                            results.Service = item["SERVICE"].InnerText;
                                            results.Recdate = item["RECDATE"].InnerText;
                                            results.Disdate = item["DISDATE"].InnerText;
                                            results.DateTimeStamp = DateTime.Now;
                                            results.Varname = child["VARNAME"].InnerText;
                                            results.Value = child["VALUE"].InnerText;
                                            _context.PatientleveldataCahpsResponse.Add(results);
                                        }
                                    }
                                    _context.SaveChanges();
                                    c.Commit();
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error("Exception occurred during the CAHPS/RESPONSE upload. :: " + ex.Message);
                                    c.Rollback();
                                    continue;
                                }
                            }
                        }
                        _resultMessage = "The CAHPS data from the XML File has been inserted into the database successfully.";
                        resultStatus = true;
                        return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Exception occurred during the CAHPS/RESPONSE upload. :: " + ex.Message);
                        _resultMessage = "There was an issue inserting the Data from the XML. The remainder of the file wasnt inserted.";
                        return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                    }
                    #endregion                   
                }
                else
                {
                    _resultMessage = "File was not uploaded. Or the file was empty. Please return to the upload page and try again.";
                    return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Exception occurred during file upload itself. :: " + ex.Message);
                _resultMessage = "File was not uploaded. Or the file was empty. Please return to the upload page and try again.";
                return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
            }
        }

        [HttpPost]
        public IActionResult UploadFileDemographics(IFormFile file)
        {
            LoggingConfiguration config = new NLog.Config.LoggingConfiguration();
            FileTarget logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"PressGaneyUploadDemographicsFromXML-{DateTime.Now:MM-dd-yyyy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;

            var _resultMessage = "";
            bool resultStatus = false;
            try
            {
                if (file.Length > 0)
                {
                    var doc = new XmlDocument();
                    doc.Load(file.OpenReadStream());

                    try
                    {
                        XmlNodeList _patientLevelDataCAHPSList = doc.SelectNodes("/DATA_EXPORT/PATIENTLEVELDATA");
                        Logger.Info("Starting DEMOGRAPHICS/RESPONSE Only from XML File.");
                        foreach (XmlNode item in _patientLevelDataCAHPSList)
                        {
                            #region Patient Level Data Demographics Response
                            using (var d = _context.Database.BeginTransaction())
                            {
                                try
                                {
                                    foreach (XmlNode newchild in item.SelectNodes("DEMOGRAPHICS/RESPONSE"))
                                    {

                                        var doesExist = _context.PatientleveldataDemographicsResponse.Where(x => x.SurveyId == item["SURVEY_ID"].InnerText && x.ClientId == item["CLIENT_ID"].InnerText && x.Service == item["SERVICE"].InnerText && x.Recdate == item["RECDATE"].InnerText && x.Disdate == item["DISDATE"].InnerText && x.Varname == newchild["VARNAME"].InnerText && x.Value == newchild["VALUE"].InnerText).FirstOrDefault();

                                        if (doesExist == null)
                                        {
                                            Logger.Info("New DEMOGRAPHICS/RESPONSE Found. VARNAME: " + newchild["VARNAME"].InnerText + " :: SURVERY_ID: " + item["SURVEY_ID"].InnerText);

                                            PatientleveldataDemographicsResponse _demographicsResults = new PatientleveldataDemographicsResponse();
                                            _demographicsResults.SurveyId = item["SURVEY_ID"].InnerText;
                                            _demographicsResults.ClientId = item["CLIENT_ID"].InnerText;
                                            _demographicsResults.Service = item["SERVICE"].InnerText;
                                            _demographicsResults.Recdate = item["RECDATE"].InnerText;
                                            _demographicsResults.Disdate = item["DISDATE"].InnerText;
                                            _demographicsResults.DateTimeStamp = DateTime.Now;
                                            _demographicsResults.Varname = newchild["VARNAME"].InnerText;
                                            _demographicsResults.Value = newchild["VALUE"].InnerText;
                                            _context.PatientleveldataDemographicsResponse.Add(_demographicsResults);
                                        }
                                    }
                                    _context.SaveChanges();

                                    d.Commit();
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error("Exception occurred during the DEMOGRAPHICS/RESPONSE upload. :: " + ex.Message);

                                    d.Rollback();
                                    continue;
                                }
                            }
                            #endregion
                        }
                        _resultMessage = "The Demographics data from the XML File has been inserted into the database successfully.";
                        resultStatus = true;
                        return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Exception occurred during the DEMOGRAPHICS/RESPONSE upload. :: " + ex.Message);
                        _resultMessage = "There was an issue inserting the Data from the XML. The remainder of the file wasnt inserted.";
                        return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                    }
                }
                else
                {
                    _resultMessage = "File was not uploaded. Or the file was empty. Please return to the upload page and try again.";
                    return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Exception occurred during file upload itself. :: " + ex.Message);
                _resultMessage = "File was not uploaded. Or the file was empty. Please return to the upload page and try again.";
                return RedirectToAction("ResultsPage", "Upload", new { _resultMessage, resultStatus });
            }
        }
    }
}