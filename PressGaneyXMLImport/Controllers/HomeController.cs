﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PressGaneyXMLImport.Models;

namespace PressGaneyXMLImport.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly PressGaneyContext _context;

        public HomeController(ILogger<HomeController> logger, PressGaneyContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult CAHPSResults()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GETCAHPSResults()
        {
            var jsonData = "";

            var _cahpsResults = _context.UvCahpsResults.ToList();
            jsonData = JsonConvert.SerializeObject(_cahpsResults);

            var jsonResult = Json(new { data = jsonData });
            return jsonResult;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
