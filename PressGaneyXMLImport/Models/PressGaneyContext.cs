﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PressGaneyXMLImport.Models
{
    public partial class PressGaneyContext : DbContext
    {
        public PressGaneyContext()
        {
        }

        public PressGaneyContext(DbContextOptions<PressGaneyContext> options)
            : base(options)
        {
        }

        public virtual DbSet<PatientleveldataCahpsResponse> PatientleveldataCahpsResponse { get; set; }
        public virtual DbSet<PatientleveldataDemographicsResponse> PatientleveldataDemographicsResponse { get; set; }
        public virtual DbSet<QuestionMap> QuestionMap { get; set; }
        public virtual DbSet<UvCahpsResults> UvCahpsResults { get; set; }
        public virtual DbSet<XmlPgData> XmlPgData { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PatientleveldataCahpsResponse>(entity =>
            {
                entity.ToTable("PATIENTLEVELDATA_CAHPS_RESPONSE", "dbo");

                entity.Property(e => e.ClientId)
                    .HasColumnName("CLIENT_ID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateTimeStamp)
                    .HasColumnName("DATE_TIME_STAMP")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Disdate)
                    .HasColumnName("DISDATE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Recdate)
                    .HasColumnName("RECDATE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Service)
                    .HasColumnName("SERVICE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SurveyId)
                    .HasColumnName("SURVEY_ID")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Value)
                    .HasColumnName("VALUE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Varname)
                    .HasColumnName("VARNAME")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PatientleveldataDemographicsResponse>(entity =>
            {
                entity.ToTable("PATIENTLEVELDATA_DEMOGRAPHICS_RESPONSE", "dbo");

                entity.Property(e => e.ClientId)
                    .HasColumnName("CLIENT_ID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateTimeStamp)
                    .HasColumnName("DATE_TIME_STAMP")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Disdate)
                    .HasColumnName("DISDATE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Recdate)
                    .HasColumnName("RECDATE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Service)
                    .HasColumnName("SERVICE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SurveyId)
                    .HasColumnName("SURVEY_ID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasColumnName("VALUE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Varname)
                    .HasColumnName("VARNAME")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<QuestionMap>(entity =>
            {
                entity.HasKey(e => new { e.Service, e.Varname })
                    .HasName("PK_QUESTION_MAP_1");

                entity.ToTable("QUESTION_MAP", "dbo");

                entity.Property(e => e.Service)
                    .HasColumnName("SERVICE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Varname)
                    .HasColumnName("VARNAME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateTimeStamp)
                    .HasColumnName("DATE_TIME_STAMP")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.QuestionText)
                    .HasColumnName("QUESTION_TEXT")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Screening)
                    .HasColumnName("SCREENING")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Section)
                    .HasColumnName("SECTION")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Standard)
                    .HasColumnName("STANDARD")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TopBoxAnswer)
                    .HasColumnName("TOP_BOX_ANSWER")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TopBoxScale)
                    .HasColumnName("TOP_BOX_SCALE")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UvCahpsResults>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("uv_CAHPS_Results", "dbo");

                entity.Property(e => e.ClientId)
                    .HasColumnName("CLIENT_ID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DateTimeStamp)
                    .HasColumnName("DATE_TIME_STAMP")
                    .HasColumnType("datetime");

                entity.Property(e => e.Disdate)
                    .HasColumnName("DISDATE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Itacctno)
                    .HasColumnName("ITACCTNO")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.QmQuestionTxt)
                    .HasColumnName("QM_QUESTION_TXT")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.QmScreening)
                    .HasColumnName("QM_SCREENING")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.QmSection)
                    .HasColumnName("QM_SECTION")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.QmService)
                    .HasColumnName("QM_SERVICE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.QmStandard)
                    .HasColumnName("QM_STANDARD")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.QmTopBoxAnswer)
                    .HasColumnName("QM_TOP_BOX_ANSWER")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.QmTopBoxScale)
                    .HasColumnName("QM_TOP_BOX_SCALE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.QmVarname)
                    .IsRequired()
                    .HasColumnName("QM_VARNAME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Recdate)
                    .HasColumnName("RECDATE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Service)
                    .HasColumnName("SERVICE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SurveyId)
                    .HasColumnName("SURVEY_ID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasColumnName("VALUE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Varname)
                    .HasColumnName("VARNAME")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<XmlPgData>(entity =>
            {
                entity.ToTable("XML_PG_Data", "dbo");

                entity.Property(e => e.LoadedDateTime).HasColumnType("datetime");

                entity.Property(e => e.Xmldata)
                    .HasColumnName("XMLData")
                    .HasColumnType("xml");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
