﻿using System;
using System.Collections.Generic;

namespace PressGaneyXMLImport.Models
{
    public partial class UvCahpsResults
    {
        public string SurveyId { get; set; }
        public string ClientId { get; set; }
        public string Service { get; set; }
        public string Recdate { get; set; }
        public string Disdate { get; set; }
        public string Varname { get; set; }
        public string Value { get; set; }
        public string Itacctno { get; set; }
        public string QmService { get; set; }
        public string QmVarname { get; set; }
        public string QmQuestionTxt { get; set; }
        public string QmSection { get; set; }
        public string QmStandard { get; set; }
        public string QmScreening { get; set; }
        public string QmTopBoxScale { get; set; }
        public string QmTopBoxAnswer { get; set; }
        public DateTime? DateTimeStamp { get; set; }
    }
}
