﻿using System;
using System.Collections.Generic;

namespace PressGaneyXMLImport.Models
{
    public partial class XmlPgData
    {
        public int Id { get; set; }
        public string Xmldata { get; set; }
        public DateTime? LoadedDateTime { get; set; }
    }
}
