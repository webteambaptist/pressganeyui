﻿using System;
using System.Collections.Generic;

namespace PressGaneyXMLImport.Models
{
    public partial class QuestionMap
    {
        public string Service { get; set; }
        public string Varname { get; set; }
        public string QuestionText { get; set; }
        public string Section { get; set; }
        public string Standard { get; set; }
        public string Screening { get; set; }
        public string TopBoxScale { get; set; }
        public string TopBoxAnswer { get; set; }
        public DateTime? DateTimeStamp { get; set; }
    }
}
