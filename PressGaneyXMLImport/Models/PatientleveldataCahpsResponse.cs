﻿using System;
using System.Collections.Generic;

namespace PressGaneyXMLImport.Models
{
    public partial class PatientleveldataCahpsResponse
    {
        public int Id { get; set; }
        public string SurveyId { get; set; }
        public string ClientId { get; set; }
        public string Service { get; set; }
        public string Recdate { get; set; }
        public string Disdate { get; set; }
        public string Varname { get; set; }
        public string Value { get; set; }
        public DateTime? DateTimeStamp { get; set; }
    }
}
